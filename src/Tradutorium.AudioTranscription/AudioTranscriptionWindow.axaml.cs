using Avalonia.Controls;
using System;
using System.Threading.Tasks;
using Whisper.net.Ggml;
using Whisper.net;
using System.IO;
using System.Linq;
using NAudio.Wave.SampleProviders;
using NAudio.Wave;
using MsBox.Avalonia.Enums;
using MsBox.Avalonia;
using System.Globalization;
using System.Threading;
using MsBox.Avalonia.Dto;

namespace Tradutorium.AudioTranscription;

public partial class MainWindow : Window
{
    public MainWindow()
    {
        InitializeComponent();


        AttachHandlers();
    }

    public MainWindow(string cultureCode)
    {
        if (cultureCode != string.Empty)
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(cultureCode);

        InitializeComponent();

        AttachHandlers();
    }

    private void AttachHandlers()
    {
        this.ButtonInsertAudio.Click += ButtonInsertAudio_Click;

        this.ButtonTranslateInput.Click += ButtonTranslateInput_Click;
    }

    public string TextToTranslate { get; set; }

    private async void ButtonInsertAudio_Click(object? sender, Avalonia.Interactivity.RoutedEventArgs e)
    {
        var selectedLanguage = this.ComboBoxInputAudioLang.SelectedItem as ComboBoxItem;

        if (selectedLanguage?.Tag != null && selectedLanguage.Tag.Equals("noLang"))
        {
            await MessageBoxManager.GetMessageBoxStandard("Caption", $"Please select the audio language first",
                    ButtonEnum.Ok).ShowAsync();
        }
        else
        {
            var filePath = await OpenFileBrowser();

            if (String.IsNullOrEmpty(filePath)) return;

            await WhisperExample(filePath, selectedLanguage.Tag.ToString());
        }
    }

    private void ButtonTranslateInput_Click(object? sender, Avalonia.Interactivity.RoutedEventArgs e)
    {
        if (this.TextBoxInput.Text != null)
            this.TextToTranslate = TextBoxInput.Text;

        this.Close();
    }

    private async Task<string> OpenFileBrowser()
    {
        var dialog = new OpenFileDialog()
        {
            Title = "Select an audio file",
            AllowMultiple = false,
        };

        dialog.Filters.Add(new FileDialogFilter() { Name = "MP3, WAV", Extensions = { "wav", "mp3" } });


        string[] result = await dialog.ShowAsync(GetWindow());

        return string.Join(" ", result);
    }

    Window GetWindow() => (Window)this.VisualRoot;


    private async Task WhisperExample(string filePath, string inputLanguage)
    {
        // This examples shows how to use Whisper.net to create a transcription from an audio file with 16Khz sample rate.

        var selectedQuality = this.ComboBoxAudioTranscriptionQuality.SelectedItem as ComboBoxItem;

        // We declare three variables which we will use later, ggmlType, modelFileName and wavFileName

        GgmlType ggmlType = GgmlType.Base;
        string modelFileName = "ggml-base.bin";

        if (selectedQuality?.Tag != null && selectedQuality.Tag.Equals("tiny"))
        {
            ggmlType = GgmlType.Base;
            modelFileName = "ggml-tiny.bin";
        }
        else if (selectedQuality?.Tag != null && selectedQuality.Tag.Equals("base"))
        {
            ggmlType = GgmlType.Base;
            modelFileName = "ggml-base.bin";
        }
        else if (selectedQuality?.Tag != null && selectedQuality.Tag.Equals("small"))
        {
            ggmlType = GgmlType.Small;
            modelFileName = "ggml-small.bin";
        }
        else if (selectedQuality?.Tag != null && selectedQuality.Tag.Equals("medium"))
        {
            ggmlType = GgmlType.Medium;
            modelFileName = "ggml-medium.bin";
        }
        else if (selectedQuality?.Tag != null && selectedQuality.Tag.Equals("large"))
        {
            ggmlType = GgmlType.Medium;
            modelFileName = "ggml-large.bin";
        }

        // This section detects whether the "ggml-base.bin" file exists in our project disk. If it doesn't, it downloads it from the internet
        var modelRelativeFilePath = Path.Combine("audio_transcription_models", modelFileName);

        if (!File.Exists(modelRelativeFilePath))
        {
            var messageBoxStandardWindow = MessageBoxManager.GetMessageBoxStandard(
               new MessageBoxStandardParams
               {
                   ButtonDefinitions = ButtonEnum.YesNo,
                   ContentTitle = "Model download",
                   ContentHeader = "Model not found locally",
                   ContentMessage = "The selected audio transcription model does not exist in the machine." + Environment.NewLine + 
                   "Do you want to download it?" + Environment.NewLine + 
                   "It can range from 140 MB to 2.5 GB, depending on the quality."
               });

            var modelDownloadResult = await messageBoxStandardWindow.ShowAsync();

            if (modelDownloadResult == ButtonResult.Ok || modelDownloadResult == ButtonResult.Yes)
                await DownloadModel(modelRelativeFilePath, ggmlType);
            else
                return;
        }

        // This section creates the whisperFactory object which is used to create the processor object.
        using var whisperFactory = WhisperFactory.FromPath(modelRelativeFilePath);

        // This section creates the processor object which is used to process the audio file, it uses language `auto` to detect the language of the audio file.
        using var processor = whisperFactory.CreateBuilder()
            .WithLanguageDetection()
            //.WithTranslate()
            .WithLanguage(inputLanguage)
            .Build();

        using var fileStream = File.OpenRead(filePath);

        var fileExtension = Path.GetExtension(filePath);

        using var wavStream = new MemoryStream();

        if (fileExtension != null && fileExtension.Equals(".wav"))
        {
            using var reader = new WaveFileReader(fileStream);
            var resampler = new WdlResamplingSampleProvider(reader.ToSampleProvider(), 16000);
            WaveFileWriter.WriteWavFileToStream(wavStream, resampler.ToWaveProvider16());

            // This section sets the wavStream to the beginning of the stream. (This is required because the wavStream was written to in the previous section)
            wavStream.Seek(0, SeekOrigin.Begin);
        }
        else if (fileExtension != null && fileExtension.Equals(".mp3"))
        {
            using var reader = new Mp3FileReader(fileStream);
            var resampler = new WdlResamplingSampleProvider(reader.ToSampleProvider(), 16000);
            WaveFileWriter.WriteWavFileToStream(wavStream, resampler.ToWaveProvider16());

            // This section sets the wavStream to the beginning of the stream. (This is required because the wavStream was written to in the previous section)
            wavStream.Seek(0, SeekOrigin.Begin);
        }
        else
        {
            await MessageBoxManager.GetMessageBoxStandard("Caption", $"File type is not currently supported",
        ButtonEnum.Ok).ShowAsync();
            return;
        }


        // This section processes the audio file and prints the results (start time, end time and text) to the console.
        var fullText = String.Empty;
        await foreach (var result in processor.ProcessAsync(wavStream))
        {
            fullText += result.Text;
            Console.WriteLine($"{result.Start}->{result.End}: {result.Text}");
        }
        this.TextBoxInput.Text = fullText;
        Console.WriteLine(fullText);
    }

    private static async Task DownloadModel(string fileName, GgmlType ggmlType)
    {
        Console.WriteLine($"Downloading Model {fileName}");
        using var modelStream = await WhisperGgmlDownloader.GetGgmlModelAsync(ggmlType);
        using var fileWriter = File.OpenWrite(fileName);
        await modelStream.CopyToAsync(fileWriter);
    }

}
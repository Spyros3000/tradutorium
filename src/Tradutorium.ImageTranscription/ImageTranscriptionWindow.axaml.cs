using Avalonia.Controls;
using Avalonia.Media.Imaging;
using MsBox.Avalonia;
using MsBox.Avalonia.Enums;
using SkiaSharp;
using System;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Tesseract;

namespace Tradutorium.ImageTranscription;

public partial class MainWindow : Window
{
    public MainWindow()
    {
        InitializeComponent();

        AttachHandlers();
    }

    public MainWindow(string cultureCode)
    {
        if (cultureCode != string.Empty)
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(cultureCode);

        InitializeComponent();

        AttachHandlers();
    }

    private void AttachHandlers()
    {
        this.ButtonInsertImage.Click += ButtonInsertImage_Click;

        this.ButtonTranslateInput.Click += ButtonTranslateInput_Click;
    }

    public string TextToTranslate { get; set; }

    private void ButtonTranslateInput_Click(object? sender, Avalonia.Interactivity.RoutedEventArgs e)
    {
        if (this.TextBoxInput.Text != null)
            this.TextToTranslate = TextBoxInput.Text;

        this.Close();
    }

    private async void ButtonInsertImage_Click(object? sender, Avalonia.Interactivity.RoutedEventArgs e)
    {
        var selectedLanguage = this.ComboBoxInputImageLang.SelectedItem as ComboBoxItem;

        if (selectedLanguage?.Tag != null && selectedLanguage.Tag.Equals("noLang"))
        {
            await MessageBoxManager.GetMessageBoxStandard("Caption", $"Please select the image language first",
                    ButtonEnum.Ok).ShowAsync();
        }
        else
        {
            var filePath = await OpenFileBrowser();

            await using (var imageStream = await LoadImageAsync(filePath))
            {
                if (imageStream is MemoryStream) return;

                var img = await Task.Run(() => Bitmap.DecodeToHeight(imageStream, 200));
                this.ImageDisplay.Source = img;
            }

            await ConvertImageToText(filePath, selectedLanguage.Tag.ToString());
        }
    }

    private async Task<string> OpenFileBrowser()
    {
        var dialog = new OpenFileDialog()
        {
            Title = "Select an image file",
            AllowMultiple = false,
        };

        dialog.Filters.Add(new FileDialogFilter() { Name = "BMP, JPG, PNG, WEBP", Extensions = { "bmp", "jpg", "png", "webp" } });


        string[] result = await dialog.ShowAsync(GetWindow());

        return string.Join(" ", result);
    }

    Window GetWindow() => (Window)this.VisualRoot;



    public async Task<Stream> LoadImageAsync(string filePath)
    {
        if (File.Exists(filePath))
        {
            return File.OpenRead(filePath);
        }
        else
        {
            return new MemoryStream();
        }
    }

    private async Task ConvertImageToText(string filePath, string inputLanguage)
    {
        var fullText = String.Empty;

        try
        {
            using (var engine = new TesseractEngine(@"./tesseract_models", inputLanguage, EngineMode.Default))
            {
                using (var img = Pix.LoadFromFile(filePath))
                {
                    using (var page = engine.Process(img))
                    {
                        var text = page.GetText();
                        Console.WriteLine("Mean confidence: {0}", page.GetMeanConfidence());

                        fullText += text;
                        fullText += System.Environment.NewLine;

                        Console.WriteLine("Text (GetText): \r\n{0}", text);
                        Console.WriteLine("Text (iterator):");
                        using (var iter = page.GetIterator())
                        {
                            iter.Begin();

                            do
                            {
                                do
                                {
                                    do
                                    {
                                        do
                                        {
                                            if (iter.IsAtBeginningOf(PageIteratorLevel.Block))
                                            {
                                                Console.WriteLine("<BLOCK>");
                                            }

                                            Console.Write(iter.GetText(PageIteratorLevel.Word));
                                            Console.Write(" ");

                                            if (iter.IsAtFinalOf(PageIteratorLevel.TextLine, PageIteratorLevel.Word))
                                            {
                                                Console.WriteLine();
                                            }
                                        } while (iter.Next(PageIteratorLevel.TextLine, PageIteratorLevel.Word));

                                        if (iter.IsAtFinalOf(PageIteratorLevel.Para, PageIteratorLevel.TextLine))
                                        {
                                            Console.WriteLine();
                                        }
                                    } while (iter.Next(PageIteratorLevel.Para, PageIteratorLevel.TextLine));
                                } while (iter.Next(PageIteratorLevel.Block, PageIteratorLevel.Para));
                            } while (iter.Next(PageIteratorLevel.Block));
                        }
                    }
                }
            }
        }
        catch (Exception e)
        {
            Trace.TraceError(e.ToString());
            Console.WriteLine("Unexpected Error: " + e.Message);
            Console.WriteLine("Details: ");
            Console.WriteLine(e.ToString());
        }

        this.TextBoxInput.Text = fullText;
    }


}
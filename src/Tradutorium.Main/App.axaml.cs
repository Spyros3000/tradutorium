using Avalonia;
using Avalonia.Controls;
using Avalonia.Controls.ApplicationLifetimes;
using Avalonia.Markup.Xaml;
using System;
using System.Globalization;
using System.IO;
using System.Linq;
using static System.Runtime.InteropServices.JavaScript.JSType;

namespace Tradutorium.Main;

public partial class App : Application
{
    internal static System.Collections.Generic.List<Models.LanguageCodes> LanguageCodes;

    public override void Initialize()
    {
        AvaloniaXamlLoader.Load(this);
    }

    internal static string GetSelectedLanguage()
    {
        var workingDirectory = Directory.GetCurrentDirectory();

        var languageConfig = Path.Combine(workingDirectory, "config", "lang.txt");

        var languageCode = string.Empty;

        if (File.Exists(languageConfig))
            languageCode = File.ReadAllText(languageConfig);

        bool cultureExistsInAssembly = false;

        if (languageCode != System.String.Empty)
            cultureExistsInAssembly = FindIfCultureExists(languageCode);

        if (cultureExistsInAssembly)
            return languageCode;
        else
            return string.Empty;
    }

    private static bool FindIfCultureExists(string cultureCode)
    {
        return Array.Exists(ThisAssembly.SupportedCultures, element => element == cultureCode);
    }

    //https://stackoverflow.com/questions/13723790/how-to-check-if-culture-exists-in-net
    private static bool DoesCultureExist(string cultureName)
    {
        return CultureInfo.GetCultures(CultureTypes.AllCultures).Any(culture => string.Equals(culture.Name, cultureName, StringComparison.CurrentCultureIgnoreCase));
    }

    public override async void OnFrameworkInitializationCompleted()
    {
        App.LanguageCodes = await Utilities.CsvParsing.ParseCsv();

        var languageCode = GetSelectedLanguage();
        if (languageCode != string.Empty)
            Properties.Resources.Culture = new CultureInfo(languageCode);

        if (ApplicationLifetime is IClassicDesktopStyleApplicationLifetime desktop)
        {
            desktop.MainWindow = new MainWindow();
        }

        base.OnFrameworkInitializationCompleted();
    }
}

internal static class ThisAssembly
{
    internal static readonly string[] SupportedCultures = new string[] {
            "el-GR",
            "en-US",
            "fr-FR",
            "it-IT"
        };
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MsBox.Avalonia.Enums;
using MsBox.Avalonia;
using NTextCat;

namespace Tradutorium.Main.Utilities
{
    internal static class LanguageDetection
    {
        internal async static Task<string> DetectLanguage(string inputText)
        {
            var factory = new RankedLanguageIdentifierFactory();
            var identifier = factory.Load("ntextcat_models\\Wiki82.profile.xml");
            var languages = identifier.Identify(inputText);
            var mostCertainLanguage = languages.FirstOrDefault();

            if (mostCertainLanguage != null && mostCertainLanguage.Item1.Iso639_3.Equals("simple"))
                mostCertainLanguage = languages.ElementAtOrDefault(1);

            if (mostCertainLanguage != null)
            {
                var threeLetterCode = ConvertLanguageCodes.Convert2LetterLangCodeTo3LetterLangCode(mostCertainLanguage.Item1.Iso639_3);
                await MessageBoxManager.GetMessageBoxStandard("Caption", $"The language of the text is '{threeLetterCode}' (ISO639-3 code)",
                    ButtonEnum.YesNo).ShowAsync();
                return threeLetterCode;

            }
            else
            {
                await MessageBoxManager.GetMessageBoxStandard("Caption", $"The language couldn’t be identified with an acceptable degree of certainty",
                    ButtonEnum.YesNo).ShowAsync();
                return String.Empty;
            }

        }
    }
}

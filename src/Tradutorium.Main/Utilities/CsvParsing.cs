﻿using Sylvan.Data.Csv;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tradutorium.Main.Utilities
{
    internal class CsvParsing
    {
        internal static async Task<List<Models.LanguageCodes>> ParseCsv()
        {
            var csvPath = Path.Combine(Directory.GetCurrentDirectory(), "language_codes", "language_codes.csv");

            var listOfLanguageCodes = new List<Models.LanguageCodes>();

            using var csv = CsvDataReader.Create(csvPath);

            while (await csv.ReadAsync())
            {
                var twoLetterCode = csv.GetString(0);
                var threeLetterCode = csv.GetString(1);
                var wiktionaryName = csv.GetString(2);

                var item = new Models.LanguageCodes(twoLetterCode, threeLetterCode, wiktionaryName);
                listOfLanguageCodes.Add(item);
            }

            return listOfLanguageCodes;
        }
    }
}

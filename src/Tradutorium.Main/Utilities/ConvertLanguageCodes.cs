﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tradutorium.Main.Utilities
{
    internal static class ConvertLanguageCodes
    {
        internal static string Convert3LetterLangCodeTo2LetterLangCode(string threeLetterLangCode)
        {
            var languageCodes = App.LanguageCodes;

            if (languageCodes != null)
            {
                foreach (Models.LanguageCodes code in languageCodes)
                {
                    if (code.ThreeLetterCode.Equals(threeLetterLangCode))
                        return code.TwoLetterCode;
                }
            }

            return null;
        }


        internal static string Convert2LetterLangCodeTo3LetterLangCode(string twoLetterLangCode)
        {
            var languageCodes = App.LanguageCodes;

            if (languageCodes != null)
            {
                foreach (Models.LanguageCodes code in languageCodes)
                {
                    if (code.TwoLetterCode.Equals(twoLetterLangCode))
                        return code.ThreeLetterCode;
                }
            }

            return null;
        }

        internal static string Convert3LetterLangCodeToWiktionaryLangName(string threeLetterLangCode)
        {
            var languageCodes = App.LanguageCodes;

            if (languageCodes != null)
            {
                foreach (Models.LanguageCodes code in languageCodes)
                {
                    if (code.ThreeLetterCode.Equals(threeLetterLangCode))
                        return code.WiktionaryName;
                }
            }

            return null;
        }

    }
}

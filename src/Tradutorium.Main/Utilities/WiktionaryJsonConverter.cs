﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using System.Linq.Expressions;
using System.Text.Json.Serialization.Metadata;

namespace Tradutorium.Main.Utilities
{
    internal class WiktionaryJsonConverter
    {
        internal async static Task<string?> InitializeWiktionaryQuery(string selectedWord, string threeLetterLangCode)
        {
            var lang = ConvertLanguageCodes.Convert3LetterLangCodeToWiktionaryLangName(threeLetterLangCode);// "English";

            if (!selectedWord.Equals(String.Empty))
            {
                var client = new HttpClient();

                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("User-Agent", "Tradutorium");

                var wiktionaryApiLink = $"https://en.wiktionary.org/api/rest_v1/page/definition/{selectedWord}";

                try
                {
                    var dtoString = await client.GetStringAsync(wiktionaryApiLink);

                    var listOfDto = JsonSerializer.Deserialize<AllDtos>(dtoString, SourceGenerationContext.Default.AllDtos);

                    for (int i = 0; i < listOfDto.ListOfDtos.Count; i++)
                    {
                        if (listOfDto.ListOfDtos[i].Language.Equals(lang))
                        {
                            return HtmlifyWiktionaryString(listOfDto.ListOfDtos[i].Definitions.FirstOrDefault());
                        }
                    }
                }
                catch
                {

                }

            }
            return null;
        }

        private static string? HtmlifyWiktionaryString(string? wiktionaryString)
        {

            var htmlString = wiktionaryString.Replace("<a rel=\"mw:WikiLink\" href=\"", "<a href=\"https://en.wiktionary.org");

            // Optional, title presents a tooltip
            //var pattern = " title=\".*?\"";
            //var regexedString = Regex.Replace(newString, pattern, String.Empty);

            if (!String.IsNullOrEmpty(htmlString))
                return htmlString;

            return null;
        }


    }

    [JsonSourceGenerationOptions(WriteIndented = true)]
    [JsonSerializable(typeof(AllDtos))]
    internal partial class SourceGenerationContext : JsonSerializerContext
    {
    }

    public class PropertyDtoConverter : JsonConverter<AllDtos>
    {
        public override AllDtos Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
        {
            if (reader.TokenType != JsonTokenType.StartObject)
            {
                throw new JsonException();
            }

            var dto = new PropertyDto();
            List<PropertyDto> listOfDto = new List<PropertyDto>();

            while (reader.Read())
            {
                if (reader.TokenType == JsonTokenType.EndObject && dto.Language == null)
                {
                    var allDtos = new AllDtos();
                    allDtos.ListOfDtos = listOfDto;

                    return allDtos;
                }

                if (reader.TokenType == JsonTokenType.EndObject)
                {
                    listOfDto.Add(dto);

                    dto = new PropertyDto();
                }

                if (reader.TokenType == JsonTokenType.PropertyName)
                {
                    string propName = (reader.GetString() ?? "").ToLower();
                    reader.Read();

                    switch (propName)
                    {
                        case var _ when propName.Equals(nameof(PropertyDto.PartOfSpeech).ToLower()):
                            dto.PartOfSpeech = reader.GetString();
                            break;
                        case var _ when propName.Equals(nameof(PropertyDto.Language).ToLower()):
                            dto.Language = reader.GetString();
                            break;
                        case var _ when propName.Equals(nameof(PropertyDto.Definitions).ToLower()):
                            if (JsonDocument.TryParseValue(ref reader, out JsonDocument jsonDoc))
                            {
                                var definition = ParseEachDefinition(jsonDoc.RootElement.GetRawText());

                                dto.Definitions = definition;
                            }

                            break;
                    }
                }
            }

            throw new JsonException();
        }

        public override void Write(Utf8JsonWriter writer, AllDtos value, JsonSerializerOptions options)
        {
            writer.WriteStartObject();

            //TODO if needed

            //writer.WriteString(nameof(PropertyDto.PartOfSpeech), value.PartOfSpeech);
            //writer.WriteString(nameof(PropertyDto.Language), value.Language);
            //writer.WriteString(nameof(PropertyDto.Definitions), value.Definitions);

            writer.WriteEndObject();
        }


        public List<String> ParseEachDefinition(string allDefinitions)
        {
            var listOfStrings = new List<String>();

            var options = new JsonDocumentOptions
            {
                AllowTrailingCommas = true
            };

            using (JsonDocument document = JsonDocument.Parse(allDefinitions, options))
            {
                foreach (JsonElement element in document.RootElement.EnumerateArray())
                {
                    var str = element.GetProperty("definition").ToString();
                    listOfStrings.Add(str);
                }

            }

            return listOfStrings;
        }

    }


    [JsonConverter(typeof(PropertyDtoConverter))]
    public class PropertyDto
    {
        public string PartOfSpeech { get; set; }

        public string Language { get; set; }

        public List<String> Definitions { get; set; }

        public string Definition { get; set; }
    }

    [JsonConverter(typeof(PropertyDtoConverter))]
    public class AllDtos
    {
        public List<PropertyDto> ListOfDtos { get; set; }
    }
}

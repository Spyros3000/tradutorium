using Avalonia.Controls;
using Avalonia.Platform.Storage;
using MsBox.Avalonia;
using MsBox.Avalonia.Enums;
using System;
using System.IO;

namespace Tradutorium.Main.SettingsWindow
{
    public partial class SettingsForm : Window
    {
        public SettingsForm()
        {
            InitializeComponent();

            AttachHandlers();
        }

        private void AttachHandlers()
        {
            this.ButtonSaveSettings.Click += ButtonSaveSettings_Click;
        }

        private async void ButtonSaveSettings_Click(object? sender, Avalonia.Interactivity.RoutedEventArgs e)
        {
            var workingDirectory = Directory.GetCurrentDirectory();

            string cultureCode = String.Empty;

            var selectedUiLanguage = this.ComboBoxInput.SelectedItem as ComboBoxItem;

            if (selectedUiLanguage?.Tag != null)
                cultureCode = selectedUiLanguage.Tag.ToString();

            var configDirectory = Path.Combine(workingDirectory, "config");

            if (!Directory.Exists(configDirectory))
                Directory.CreateDirectory(configDirectory);

            var languageConfig = Path.Combine(configDirectory, "lang.txt");

            if (cultureCode != String.Empty)
                await File.WriteAllTextAsync(languageConfig, cultureCode);

            await MessageBoxManager.GetMessageBoxStandard("Caption", $"You need to restart the application for changes to take effect",
        ButtonEnum.Ok).ShowAsync();

            this.Close();
        }
    }
}

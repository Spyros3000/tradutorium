using Avalonia.Controls;
using Avalonia.OpenGL;
using Avalonia.Threading;
using Avalonia.Utilities;
using MsBox.Avalonia.Enums;
using MsBox.Avalonia;
using NTextCat.Commons;
using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Runtime.InteropServices;

namespace Tradutorium.Main;

public partial class MainWindow : Window
{
    public MainWindow()
    {
        InitializeComponent();

        AttachHandlers();
    }

    private void AttachHandlers()
    {
        this.ButtonOpenFile.Click += ButtonOpenFile_Click;
        this.ButtonExitApplication.Click += ButtonExitApplication_Click;

        this.ButtonAudioTranscription.Click += ButtonAudioTranscription_Click;
        this.ButtonImageOCR.Click += ButtonImageTranscription_Click;

        this.ButtonSettings.Click += ButtonSettings_Click;
        this.ButtonAboutTradutorium.Click += ButtonAboutTradutorium_Click;

        this.TextBoxInput.PropertyChanged += TextBoxInput_PropertyChanged;
        this.TextBoxInput.TextChanged += TextBoxInput_TextChanged;

        this.ButtonTranslate.Click += ButtonTranslate_Click;

        this.TextBoxInput.DoubleTapped += TextBoxInput_DoubleTapped;
    }

    private async void TextBoxInput_DoubleTapped(object? sender, Avalonia.Input.TappedEventArgs e)
    {
        await Task.Delay(TimeSpan.FromSeconds(1));

        var selectedWords = this.TextBoxInput.SelectedText.Split(' ');

        var selectedInputItem = this.ComboBoxInput.SelectedItem as ComboBoxItem;

        if (selectedWords.Length == 1 && selectedInputItem?.Tag != null)
        {
            this.WiktionaryHtmlPanel.Text = await Utilities.WiktionaryJsonConverter.InitializeWiktionaryQuery(selectedWords[0], selectedInputItem.Tag.ToString());
            //this.AvaloniaEditWiktionary.Text = this.LabelWiktionary.Text;
            //this.WiktionaryHtmlPanel.Text = this.LabelWiktionary.Text;
        }

    }

    private void ButtonExitApplication_Click(object? sender, Avalonia.Interactivity.RoutedEventArgs e)
    {
    }

    private void TextBoxInput_TextChanged(object? sender, TextChangedEventArgs e)
    {
    }

    private void TextBoxInput_PropertyChanged(object? sender, Avalonia.AvaloniaPropertyChangedEventArgs e)
    {
        //this.LabelWiktionary.Text = selectedWords[0];

        //if (!selectedWords[0].Equals(" ") && !selectedWords[0].Equals(String.Empty))
        //    this.LabelWiktionary.Text = selectedWords[0];
        //else if (selectedWords.Length > 1 && selectedWords[1].Length > 1)
        //    this.LabelWiktionary.Text = selectedWords[1];

        //this.LabelWiktionary.Text = this.TextBoxInput.SelectedText;
    }

    private async void ButtonOpenFile_Click(object? sender, Avalonia.Interactivity.RoutedEventArgs e)
    {
        var filePath = await OpenFileBrowser();

        if (String.IsNullOrEmpty(filePath)) return;

        this.TextBoxInput.Text = File.ReadAllText(filePath);
    }

    private async void ButtonImageTranscription_Click(object? sender, Avalonia.Interactivity.RoutedEventArgs e)
    {
        //TODO: using

        var form = new Tradutorium.ImageTranscription.MainWindow(App.GetSelectedLanguage());

        await form.ShowDialog(this);

        this.TextBoxInput.Text = form.TextToTranslate;
    }

    private async void ButtonAudioTranscription_Click(object? sender, Avalonia.Interactivity.RoutedEventArgs e)
    {
        //TODO: using

        var form = new Tradutorium.AudioTranscription.MainWindow(App.GetSelectedLanguage());

        await form.ShowDialog(this);

        this.TextBoxInput.Text = form.TextToTranslate;

        //if (await form.ShowDialog(this) == true)
        //    this.Title = form.myText();

        //if (form.ShowDialog(this) == true)
    }
    private async void ButtonSettings_Click(object? sender, Avalonia.Interactivity.RoutedEventArgs e)
    {
        var form = new SettingsWindow.SettingsForm();

        await form.ShowDialog(this);
    }

    private async void ButtonAboutTradutorium_Click(object? sender, Avalonia.Interactivity.RoutedEventArgs e)
    {
        var form = new AboutWindow.AboutForm();

        await form.ShowDialog(this);
    }

    private string OutputText { get; set; }

    private async void ButtonTranslate_Click(object? sender, Avalonia.Interactivity.RoutedEventArgs e)
    {
        OutputText = String.Empty;
        this.LabelPivot.Text = String.Empty;

        string inputLanguageCode = String.Empty;

        if (!String.IsNullOrEmpty(this.TextBoxInput.Text))
            inputLanguageCode = await Tradutorium.Main.Utilities.LanguageDetection.DetectLanguage(this.TextBoxInput.Text);

        ShowDetectedLanguageInDropdown(inputLanguageCode);

        var tempFileName = Path.GetTempFileName();
        File.WriteAllText(tempFileName, this.TextBoxInput.Text);

        var workingDirectory = Directory.GetCurrentDirectory();

        string modelConfigRelativePath = GetModelConfigRelativePath(inputLanguageCode);

        if (!File.Exists(Path.Combine(workingDirectory, modelConfigRelativePath)))
        {
            var Language1ToPivotLanguageRelativePath = GetLang1ToPivotLangModelConfigRelativePath(inputLanguageCode);
            var PivotLanguageToLanguage2RelativePath = GetPivotLangToLang2ModelConfigRelativePath();

            if (File.Exists(Path.Combine(workingDirectory, Language1ToPivotLanguageRelativePath)) &&
                File.Exists(Path.Combine(workingDirectory, PivotLanguageToLanguage2RelativePath)))
            {
                await TranslateInputTextToOutput(tempFileName, workingDirectory, Language1ToPivotLanguageRelativePath);

                var tempPivotToLang2FileName = Path.GetTempFileName();
                File.WriteAllText(tempPivotToLang2FileName, this.OutputText);
                this.OutputText = string.Empty;

                await TranslateInputTextToOutput(tempPivotToLang2FileName, workingDirectory, PivotLanguageToLanguage2RelativePath);

                // Pivot label

                var selectedInputItem = this.ComboBoxInput.SelectedItem as ComboBoxItem;

                var selectedOutputItem = this.ComboBoxOutput.SelectedItem as ComboBoxItem;

                if (selectedInputItem?.Tag != null && selectedOutputItem?.Tag != null)
                {
                    this.LabelPivot.Text = $"{selectedInputItem.Tag.ToString()} -> eng -> {selectedOutputItem.Tag.ToString()}";
                }

            }
            else
                await MessageBoxManager.GetMessageBoxStandard("Caption", $"The language pair does not exist",
                        ButtonEnum.Ok).ShowAsync();
        }
        else
        {
            await TranslateInputTextToOutput(tempFileName, workingDirectory, modelConfigRelativePath);
        }

        this.TextBoxOutput.Text = OutputText;

        if (File.Exists(tempFileName))
        {
            File.Delete(tempFileName);
        }

    }

    private async Task TranslateInputTextToOutput(string tempFileName, string workingDirectory, string modelConfigRelativePath)
    {
        if (System.Runtime.InteropServices.RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
            await TranslateInputTextToOutputWindows(tempFileName, workingDirectory, modelConfigRelativePath);
        else
            await TranslateInputTextToOutputUnix(workingDirectory, modelConfigRelativePath);
    }

        private async Task TranslateInputTextToOutputWindows(string tempFileName, string workingDirectory, string modelConfigRelativePath)
    {
        ProcessStartInfo startInfo = new ProcessStartInfo
        {
            FileName = Environment.ExpandEnvironmentVariables("%COMSPEC%"),
            //FileName = pathToExe,
            //Arguments = @"/C C:\Users\DESPI\Spyros\source\Avalonia\MyApp\bin\Debug\net6.0\bin-external\bergamot.exe --model-config-paths ende.student.tiny11\\config.intgemm8bitalpha.yml.bergamot.yml --log-level debug --cpu-threads 4 < " + tempFileName,
            //Arguments = $"/C {workingDirectory}\\bergamot.exe --model-config-paths language_models\\ende.student.tiny11\\config.intgemm8bitalpha.yml.bergamot.yml --log-level debug --cpu-threads 4 < {tempFileName}",
            Arguments = $"/C {workingDirectory}\\bergamot.exe --model-config-paths {modelConfigRelativePath} --log-level debug --cpu-threads 4 < {tempFileName}",
            UseShellExecute = false,
            CreateNoWindow = true,
            WorkingDirectory = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location),
            StandardOutputEncoding = Encoding.UTF8,
            RedirectStandardInput = true,
            RedirectStandardOutput = true,
            RedirectStandardError = true
        };

        var process = new Process();
        process.StartInfo = startInfo;

        process.OutputDataReceived += new DataReceivedEventHandler(OutputHandler);
        //process.ErrorDataReceived += new DataReceivedEventHandler(OutputHandler);

        process.Start();

        //using (var streamReader = new StreamReader(inputFilePath))
        //{
        //    process.StandardInput.Write(streamReader.ReadToEnd());
        //    process.StandardInput.Flush();
        //}

        //process.BeginOutputReadLine();
        //var output = process.StandardOutput.ReadToEnd();
        //var errors = process.StandardError.ReadToEnd();

        process.BeginOutputReadLine();
        process.BeginErrorReadLine();

        await process.WaitForExitAsync();

        //string output = process.StandardOutput.ReadToEnd();
        //var errors = process.StandardError.ReadToEnd();
    }

    private async Task TranslateInputTextToOutputUnix(string workingDirectory, string modelConfigRelativePath)
    {
        ProcessStartInfo startInfo = new ProcessStartInfo
        {
            FileName = Environment.ExpandEnvironmentVariables("%COMSPEC%"),
            Arguments = $"/C {workingDirectory}\\bergamot.exe --model-config-paths {modelConfigRelativePath} --log-level debug --cpu-threads 4 <<< {this.TextBoxInput.Text}",
            UseShellExecute = false,
            CreateNoWindow = true,
            WorkingDirectory = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location),
            StandardOutputEncoding = Encoding.UTF8,
            RedirectStandardInput = true,
            RedirectStandardOutput = true,
            RedirectStandardError = true
        };

        var process = new Process();
        process.StartInfo = startInfo;

        process.OutputDataReceived += new DataReceivedEventHandler(OutputHandler);

        process.Start();

        process.BeginOutputReadLine();
        process.BeginErrorReadLine();

        await process.WaitForExitAsync();
    }

    private string GetModelConfigRelativePath(string inputLanguageCode)
    {
        var selectedOutputItem = this.ComboBoxOutput.SelectedItem as ComboBoxItem;
        var outputLanguage = Tradutorium.Main.Utilities.ConvertLanguageCodes.Convert3LetterLangCodeTo2LetterLangCode(selectedOutputItem.Tag.ToString());

        var inputLanguage = Tradutorium.Main.Utilities.ConvertLanguageCodes.Convert3LetterLangCodeTo2LetterLangCode(inputLanguageCode);

        var modelConfigRelativePath = $"language_models\\{inputLanguage}{outputLanguage}\\config.{inputLanguage}{outputLanguage}.bergamot.yml";
        return modelConfigRelativePath;
    }

    private string GetLang1ToPivotLangModelConfigRelativePath(string inputLanguageCode)
    {
        var inputLanguage = Tradutorium.Main.Utilities.ConvertLanguageCodes.Convert3LetterLangCodeTo2LetterLangCode(inputLanguageCode);

        var modelConfigRelativePath = $"language_models\\{inputLanguage}en\\config.{inputLanguage}en.bergamot.yml";
        return modelConfigRelativePath;
    }

    private string GetPivotLangToLang2ModelConfigRelativePath()
    {
        var selectedOutputItem = this.ComboBoxOutput.SelectedItem as ComboBoxItem;
        var outputLanguage = Tradutorium.Main.Utilities.ConvertLanguageCodes.Convert3LetterLangCodeTo2LetterLangCode(selectedOutputItem.Tag.ToString());

        var modelConfigRelativePath = $"language_models\\en{outputLanguage}\\config.en{outputLanguage}.bergamot.yml";
        return modelConfigRelativePath;
    }


    private async void OutputHandler(object sender, DataReceivedEventArgs e)
    {
        OutputText += e.Data;

        //TODO
        //await Dispatcher.UIThread.InvokeAsync(new Action(() => { this.TextBoxOutput.Text = output; }), DispatcherPriority.Default);
        //this.TextBoxOutput.Text = output;
    }

    private void ShowDetectedLanguageInDropdown(string languageCode)
    {
        if (!String.IsNullOrEmpty(languageCode))
        {
            foreach (var item in this.ComboBoxInput.Items)
            {
                var detectedLanguageItem = item as ComboBoxItem;
                if (detectedLanguageItem?.Tag != null && detectedLanguageItem.Tag.Equals(languageCode))
                    this.ComboBoxInput.SelectedItem = item;
            }
        }
    }

    private async Task<string> OpenFileBrowser()
    {
        var dialog = new OpenFileDialog()
        {
            Title = "Select a text file",
            AllowMultiple = false,
        };

        dialog.Filters.Add(new FileDialogFilter() { Name = "TXT", Extensions = { "txt" } });


        string[] result = await dialog.ShowAsync(GetWindow());

        return string.Join(" ", result);
    }

    Window GetWindow() => (Window)this.VisualRoot;
}
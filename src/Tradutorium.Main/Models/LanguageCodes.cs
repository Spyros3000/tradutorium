﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tradutorium.Main.Models
{
    internal class LanguageCodes
    {
        public string TwoLetterCode { get; set; }
        public string ThreeLetterCode { get; set; }
        public string WiktionaryName { get; set; }

        public LanguageCodes(string twoLetterCode, string threeLetterCode, string wiktionaryName)
        {
            this.TwoLetterCode = twoLetterCode;
            this.ThreeLetterCode = threeLetterCode;
            this.WiktionaryName = wiktionaryName;
        }

    }
}

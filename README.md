# Tradutorium

Tradutorium supports

* offline machine translation (using project Bergamot)
* audio transcription (using Whisper)
* image transcription (using Tesseract)
* Wiktionary lookup (through the official API)

Tradutorium currently has been tested under Windows. Linux support is planned, macOS could possibly be added in the future if there is access to the necessary hardware. It is made available under the Mozilla Public License.

## Application hierarchy

At each current state Tradutorium consists of three different projects that function together:

* Tradutorium.Main: is the central pillar of the application and also handles translation duties, depends on the other two projects.

* Tradutorium.AudioTranscription: handles audio transcription

* Tradutorium.ImageTranscription: handles image transcription

## Dependencies

Bergamot Translator needs to be compiled first in order for Tradutorium to function. This in turn has dependencies to other libraries, like Intel MKL or OpenBLAS. Details can be found on the official repository and in the paper "Tradutorium"...

This is the only dependency with unmanaged code. All other dependencies are written in .NET languages or have .NET bindings and can be managed through Nuget.

## Compiling from source

Localizations are available in the l10n directory. The respective resource files can be generated with the resgen tool and then copied in the Resources directory.

Binaries are generated using NativeAOT. As a result, a .NET installation is NOT required to use Tradutorium. However, .NET 8 is needed in order for this feature to work, limiting IDE support to Visual Studio 2022+ and Jetbrains Rider 2023.3+.
